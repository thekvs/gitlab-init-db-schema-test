CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE TABLE seasons
(
    id integer NOT NULL CONSTRAINT seasons_pk PRIMARY KEY,
    dates_range daterange NOT NULL,
    comments    text DEFAULT ''::text
);

CREATE INDEX seasons_dates_range ON seasons USING gist (dates_range);
INSERT INTO seasons (id, dates_range, comments) VALUES (1, daterange('2020-10-01', NULL), 'Сезон 2020-2021');

